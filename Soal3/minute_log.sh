#!/bin/bash
mkdir -p  ~/log

NOW=$(date +"%Y%m%d%H%M%S")

LOGFILE="metrics_$NOW.log"

touch $LOGFILE

free -m |awk '{out="mem_"$1; for(i=2;i<=NF;i++){out=out",mem_"$i} ; if(NR==1) printf "%s", out}'> $LOGFILE
free -m |awk '{out="swap_"$1; for(i=2;i<=3;i++){out=out",swap_"$i} if(NR==1) printf "%s", out}' >> $LOGFILE
printf "path,path_size\n">> $LOGFILE
free -m | awk '{out=$2; for(i=3;i<=NF;i++){out=out","$i} ;if(NR>1) printf "%s", out}' >> $LOGFILE
du -sh ~|awk '{print ","$2"/,"$1}'>> $LOGFILE

chown $USER $LOGFILE 
mv $LOGFILE ~/log
(crontab -l; echo "* * * * * "~/minute_log.sh) | awk '!x[$0]++' | crontab -

