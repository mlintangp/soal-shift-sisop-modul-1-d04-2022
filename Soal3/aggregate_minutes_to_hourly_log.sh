#!/bin/bash
mkdir -p  ~/log

NOW=$(date +"%Y%m%d%H");

LOGFILE="metrics_agg_$NOW.log";

cat ~/log/metrics_2*.log | awk 'FS="," {if(NR%2==0){
  if(NR==2){
        for(i=1;i<=NF;i++) {
                min[i]=$i;max[i]=$i;avg[i]=$i
        }

  }else
  {
        for(j=1;j<=NF;j++){
                if(min[j]>$j){
                        min[j]=$j
                }
                if(max[j]<$j){
                        max[j]=$j
                }
                if(j!=9)
                        avg[j]=avg[j]+$j
        }
  }
}};
END{
for(line=1;line<=3;line++){
        for(k=1; k<=NF; k++){
                if(k!=NF){
                        if(line==1)
                                printf "%s,",min[k]
                        else if(line==2)
                                printf "%s,",max[k]
                        else if(line==3 && k!=9)
                                printf "%.2f;",(avg[k]/(NR/2))
			else if(line==3 && k==9)
				printf "%s;", avg[k]
                }else{
                        if(line==1)
                                printf "%s ",min[k]
                        else if(line==2)
                                printf "%s ",max[k]
                        else if(line==3 && k!=9)
                                printf "%.2f ", (avg[k]/(NR/2))
                }
        }

}}' |
awk 'END{print "minimum,"$1,"\nmaximum,"$2,"\navarage;"$3}' > temp.txt

touch $LOGFILE

free -m |awk '{out="mem_"$1; for(i=2;i<=NF;i++){out=out",mem_"$i} if(NR==1) printf "type,%s", out}' > $LOGFILE
free -m |awk '{out="swap_"$1; for(i=2;i<=3;i++){out=out",swap_"$i} if(NR==1) printf "%s;",out}'>> $LOGFILE
printf "path,path_size\n">> $LOGFILE
awk '{FS=OFS=";"} {if(NR==3) gsub(/\,/, ".", $0)} 1' temp.txt |
awk '{FS=OFS=";"} {if(NR==3) gsub(/\;/, ",", $0)} 1' >> $LOGFILE

rm temp.txt

chown $USER $LOGFILE 
mv $LOGFILE ~/log

(crontab -l ; echo "0 * * * * "~/aggregate_minutes_to_hourly_log.sh) | awk '!x[$0]++' | crontab -