# Soal Shift Sistem Operasi Modul 1 Kelompok D04 2022

## Anggota Kelompok

<table>
    <tr>
        <th>NRP</th>
        <th>Nama</th>
    </tr>
    <tr>
        <td>Muhammad Lintang Panjerino</td>
        <td>5025201045</td>
    </tr>
    <tr>
        <td>Muhammad Yunus</td>
        <td>5025201171</td>
    </tr>
    <tr>
        <td>PEDRO T KORWA</td>
        <td>05111940007003</td>
    </tr>
<table>

# Lapres Modul 1

# 1. Soal 1

**Deskripsi Soal**

Pada soal ini diberikan perintah untuk membuat sistem register di script register.sh dan sistem login di script main.sh. 

Password pada login dan register harus hidden agar aman dan harus memenuhi kriteria berikut:
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumeric
- Tidak boleh sama dengan username

Percobaan login dan register harus selalu tercatat pada file log.txt dengan format ```MM/DD/YY hh:mm:ss MESSAGE``` dengan detail kalimat **MESSAGE** adalah sebagai berikut:
- ```REGISTER: ERROR User already exists``` ketika mencoba register dengan username yang sudah terdaftar
- ```REGISTER: INFO User USERNAME registered successfully``` ketika percobaan register berhasil
- ```LOGIN: ERROR Failed login attempt on user USERNAME``` ketika user mencoba login tapi passwordnya salah
- ```LOGIN: INFO User USERNAME logged in``` ketika user berhasil login

Ada 2 hal yang dapat dilakukan user jika sudah berhasil login:
1. Mengetik command **dl N** untuk download gambar dari web [https://loremflickr.com/320/240](https://loremflickr.com/320/240) dan hasil download akan disimpan ke dalam folder dengan nama **YYYY-MM-DD**_**USERNAME**. Gambar yang didownload memiliki format nama **PIC_XX**. Kemudian folder akan dizip dengan nama yang sama dengan folder dan dipassword sesuai dengan password user itu. Jika sudah ada file zip dengan nama yang sama maka akan diunzip dulu, menambahkan gambar hasil download ke folder, kemudian melakukan zip ulang.
2. Mengetik command **att** untuk menampilkan percobaan login yang berhasil dan gagal dari user yang sedang login.

## 1a)

Membuat folder *users* kemudian membuat file *user.txt* di dalamnya. Di file *user.txt* digunakan untuk mencatat semua user yang telah berhasil melakukan register.
```
if [ ! -d "users/" ]
then
 mkdir users
 cd users/
 touch user.txt
 echo "$username  $password" >> user.txt
else
 cd users/
 echo "$username  $password" >> user.txt
fi
```

## 1b)

Mengatur password pada login dan register menjadi hidden.
```
read -s -p "Enter password : " password
```
Melakukan pengecekan password agar sesuai dengan kriteria.
```
elif [ $password == $username ]; then
	echo "REGISTER: ERROR Password does not meet criteria"
	date '+%m/%d/%y %H:%M:%S' >> log.txt
	echo "REGISTER: ERROR Password does not meet criteria" >> log.txt
	exit 1
elif [ $length -ge 8 ] && [[ ! $password =~ ^[a-zA-Z0-9]*[a-z][A-Z][a-zA-Z0-9]*$ && ! $password =~ ^[a-zA-Z0-9]*[A-Z][a-z][a-zA-Z0-9]*$ ]]; then
	echo "REGISTER: ERROR Password does not meet criteria"
	date '+%m/%d/%y %H:%M:%S' >> log.txt
	echo "REGISTER: ERROR Password does not meet criteria" >> log.txt
	exit 1
elif [ $length -lt 8 ]; then
	echo "REGISTER: ERROR Password does not meet criteria"
	date '+%m/%d/%y %H:%M:%S' >> log.txt
	echo "REGISTER: ERROR Password does not meet criteria" >> log.txt
	exit 1
```

## 1c)

Mengirim **MESSAGE** ke file *log.txt* sesuai dengan aksi yang dilakukan oleh user.
```
egrep "^$username" /etc/passwd >/dev/null
if [ $? -eq 0 ]; then
	echo "REGISTER: ERROR User already exists"
	date '+%m/%d/%y %H:%M:%S' >> log.txt
	echo "REGISTER: ERROR User already exists" >> log.txt
	exit 1
```

```
pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
useradd -m -p "$pass" "$username"
if [ $? -eq 0 ]; then
	echo "REGISTER: INFO User $username registered successfully"
	date '+%m/%d/%y %H:%M:%S' >> log.txt
	echo "REGISTER: INFO User $username registered successfully" >> log.txt
else
	echo "Failed to add a user!"
fi
```

```
read -p "Username: " username
egrep "^$username " ./users/user.txt >/dev/null

if [ $? -eq 0 ]; then
	read -s -p 'Password: ' password
	egrep "^$username  $password$" ./users/user.txt >/dev/null
	if [ ! $? -eq 0 ]; then
		echo "Incorrect!"
		date '+%m/%d/%y %H:%M:%S' >> log.txt
		echo "LOGIN: ERROR Failed login attempt on user $username" >> log.txt
		exit 1
	fi
else
	echo "Username does not exist!"
	exit 1
fi

date '+%m/%d/%y %H:%M:%S' >> log.txt
echo "LOGIN: INFO User $username logged in" >> log.txt
```
## 1d)

Download gambar dari web [https://loremflickr.com/320/240](https://loremflickr.com/320/240) kemudian melakukan *zip* dan *unzip* folder.
```
date=$(date +%Y-%m-%d)
folder=$(echo "${date}_$username")
if [ ! -f "$folder".zip ]; then
	mkdir -p $folder
	i=1
	while [ $i -le $N ]
	do
		echo -e "download ke-$i"
		if [ $i -lt 10 ]; then
			wget https://loremflickr.com/320/240 -O ./"$folder"/PIC_"0$i"
		else
			wget https://loremflickr.com/320/240 -O ./"$folder"/PIC_"$i"
		fi
		i=$((i+1))
	done
	zip -P $password -r $folder.zip $folder
	rm -r $folder
else
	unzip -P $password $folder.zip
	jml_file=$(ls $folder | wc -l)
	j=$(($jml_file + 1))
	N=$(($jml_file + $N))
	while [ $j -le $N ]
	do
		echo -e "download ke-$j"
		if [ $j -lt 10 ]; then
			wget https://loremflickr.com/320/240 -O ./"$folder"/PIC_"0$j"
		else
			wget https://loremflickr.com/320/240 -O ./"$folder"/PIC_"$j"
		fi
		j=$((j+1))
	done
	zip -P $password -r $folder.zip $folder
	rm -r $folder
fi
```

Menghitung percobaan login dari user yang sedang login.
```
berhasil=$(egrep "$username" log.txt | egrep "LOGIN: INFO" | wc -l)
echo "Percobaan login berhasil: $berhasil"

gagal=$(egrep "$username" log.txt | egrep "LOGIN: ERROR" | wc -l)
echo "Percobaan login gagal: $gagal"
```

**Kendala:** 
- Pengecekan kriteria password "Memiliki minimal 1 huruf kapital dan 1 huruf kecil"
- Bingung ketika membuat sistem login karena awalnya mengira login langsung, namun ternyata dapat dilakukan pengecekan username dan password di file ./users/user.txt
- Error ketika zip dan unzip folder yang berisi gambar yang telah didownload dari web https://loremflickr.com/320/240/


## Soal 2
#### Deskripsi Soal:
Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:
 >* gunakan awk
>* Nanti semua file-file HASIL SAJA yang akan dimasukkan ke dalam folder forensic_log_website_daffainfo_log

### 2.A
Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log mengunakan awk.
#### Pembahasan
buat file dengan nama forensic_log_website_daffainfo_log mengunakan awk dengan 
``` bash
awk '{system("mkdir -p forensic_log_website_daffainfo_log")}' 
```
yang ditulis dalam code jawaban seperti berikut
``` bash
(awk '{system("mkdir -p forensic_log_website_daffainfo_log")} END{ cnt = (NR-1)/12;
print "Rata-rata serangan adalah sebanyak",
cnt, "request per jam "}' log_website_daffainfo.log) > ratarata.txt
```
### 2.B
Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.

Agar Dapos tidak bingung saat membaca hasilnya, formatnya akan dibuat seperti ini:
 
>File ratarata.txt **[Rata-rata]** serangan adalah sebanyak rata_rata requests per jam "

#### Pembahasan
Cari jumlah banyaknya request yang ada dari file log "log_website_daffa_info.log",
setelah hasilnya didapatkan lalu dibagi dengan 12 karena request pertama dimulai dari jam 00:00 hingga jam 12:00. Oleh karena request berjalan selama 12 jam maka total semua request dibagi 12 akan menjadi rata-rata request per jam. Lalu masukan hasilnya ke file "ratarata.txt"
**Implementasi**
``` bash
(awk '{system("mkdir -p forensic_log_website_daffainfo_log")} END{ cnt = (NR-1)/12;
print "Rata-rata serangan adalah sebanyak",
cnt, "request per jam "}' log_website_daffainfo.log) > ratarata.txt
```
**Penjelasan**
``` bash 
awk 'END{ cnt = (NR-1)/12}' 
```
Dengan END  perintah akan dijalankan setelah semua line pada file dibaca. Selanjutnya ``cnt`` akan menjadi variabel yang menyimpan hasil (rata-rata req perjam) ``NR-1/12`` NR adalah line pada baris terakhir karena fungsi dijalankan saat END. sehingga NR akan sama dengan banyaknya request yang ada. Lalu dikurang 1 karena baris pertama pada file log merupakan header bukan data request. Pada akhirnya nilainya dibagi dengan 12 untuk mendapatkan banyaknya request per jam.
Print ``cnt`` denga format yang ditentukan ke file "ratarata.txt"
``` bash
print "Rata-rata serangan adalah sebanyak",
cnt, "request per jam "}' log_website_daffainfo.log) > ratarata.txt
```
### 2.C
Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.

● File result.txt 
>IP yang paling banyak mengakses server adalah:  ip_address sebanyak jumlah_request requests

#### Pembahasan
Gunakan awk, akses argumen IP pada setiap baris pada file log. lalu hitung jumlah kemunculan tiap-tiap IP nya lalu lakukan sorting secara descending, sehingga IP dengan kemunculan terbanya ada pada baris pertama, lalu dapatkan IP dan jumlah kemunculan data pada baris pertama tersebut.
**Implementasi**
 ``` bash
(awk -F\"  '{print $2}' log_website_daffainfo.log |
uniq -c | sort -rn | head -n 1 |
awk '{print "IP yang paling banyak mengakses server adalah:",$2,
"sebanyak",$1, "requests\n"}') > result.txt
 ```
 **Penjelasan**
``awk -F\"  '{print $2}' log_website_daffainfo.log ``
perintah awk diatas akan melakukan *slicing* tiap-tiap argumen dengan ``"`` sebagai delimeter/pemisah tiap-tiap argument. Dengan delimeter tersebut IP akan berada pada argument kedua oleh karena itu ``'{print $2}'`` akan mendapatkan IP dari semua baris/data.
Selanjutnya akan dilakukan proses *pipe* dengan mengunakan ``|`` . Dengan pipe perintah yang dioutputkan disebelah kiri pipe akan menjadi data/baris2 yang akan dibaca pada proses/perintah selanjutnya yang ada disebelah kanan.
``| uniq -c `` akan menghasilkan 2 macam argumen di tiap baris yang dibaca yaitu banyaknya kemunculan data yang identik, dan data identik tersebut. ouput berupa data yang uniq dari semua baris yang ada, jika ada data yang identik/sama maka hanya akan dioutpukan satu, lalu banyaknya kemunculan akan ditambahkan.
``| sort -rn `` akan melakukan sorting secara descending terhadap hasil dari perintah sebelumnya.
``| head -n 1 `` akan mengambil hasil pada baris pertama hingga sampai pada tahap ini sudah 1 baris dengan didapatkan 2 argument, argumen 1 adalah banyaknya 
kemunculan, argument ke 2 adalah IP yang mengakses
``` bash
| awk '{print "IP yang paling banyak mengakses server adalah:",$2,
"sebanyak",$1, "requests\n"}') > result.txt
```
perintah awk diatas akan mengambila output dari proses pipe sebelumnya akan dicetak hasilnya sessui format soal ke dalam result.txt. Dengan `$1`  merupakan banyaknya kemunculan dan `$2` adalah IP yang mengakses data.
### 2.D
Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl? Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.

● File result.txt IP 
> Ada **[jumlah_req_curl]** requests yang menggunakan curl sebagai user-agent
#### Pembahasan
Temukan semua baris yang memiliki kata ``curl`` lalu hitung banyaknya baris yang ada dari hasil pencarian tersebut.
**Implementasi**
``` bash
(awk -F\"  '/curl/ {print $8} ' log_website_daffainfo.log | uniq -c | 
awk '{print"ada",
$1,"request yang mengunakan curl sebagai user agent \n"}')>> result.txt
```
**Penjelasan**
``(awk -F\" '/curl/ {print $8} ' log_website_daffainfo.log ``
Perintah awk diatas akan menemukan semua baris yang mengandung kata `curl` pada barisnya, lalu cetak argumen ke delapan dari tiap barisnya (dengan delimeter `"`) yang merupakan argument untuk user-agent yang digunakan request.
``|uniq -c`` seperti sebelumnya akan menghasilkan output berupa jumlah kemunculan data uniq, dan data uniq tersebut.
``| 
awk '{print"ada",
$1,"request yang mengunakan curl sebagai user agent \n"}')>> result.txt
``
Lalu cetak hasilnya yaitu `$1` yang merupakan banyaknya kemunculan baris, cetak ke file result.txt sesuai format. 
### 2.E
Pada jam 2 pagi pada tanggal 23 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.
>● File result.txt IP 
>**[Address Jam 2 pagi]** 
>**[IP Address Jam 2 pagi]** 
>dst
#### Pembahasan
Temukan semua baris yang memiliki ``:02:`` yang menandakan baris/request tersebut dijalankan pada jam 2 pagi, lalu cetak semua IP baris/request tersebut secara uniq (yang sama dicetak sekali) lalu simpan hasilnya pada file result.txt sesuai format.
**Implemetasi**
``awk -F\"  '/\:02\:/ {print $2}' log_website_daffainfo.log |uniq >> result.txt``
**Penjelasan**
`awk -F\"  '/\:02\:/ {print $2}' log_website_daffainfo.log`
Mendapatkan semua baris yang memiliki `:02:` lalu meberikan output berupa IP baris/request tersebut.
`| uniq` melakukan pipe terhadap peritnah sebelumnya dan memberikan data uniq dari semua baris yang dibaca.
Cetak hasilnya ke result.txt.
##Tambahan
```bash
mv ratarata.txt forensic_log_website_daffainfo_log/
mv result.txt forensic_log_website_daffainfo_log/
```
pindahkan ratarata.txt dan result.txt ke folder forensiq_website_daffainfo_log yang telah dibuat sebelumnya.
### Kendala
- Bingun untuk rata-rata request perjam apakah dibagi 24 jam, atau dibagi 12 jam (lamanya request).
- Belum diajarkan cara mengunakan pipe sebelumnya
- Belum terbiasa dengan perintah bash yang *strict*

## Soal 3
#### Deskripsi Soal
Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer. 

Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh `. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh ` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

Note:
 - nama file untuk script per menit adalah minute_log.sh 
 - nama file untuk script agregasi per jam adalah aggregate_minutes_to_hourly_log.sh 
 - semua file log terletak di /home/{user}/log

### 3.A
Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.
#### Pembahasan
``` bash
mkdir -p ~/log
NOW=$(date +"%Y%m%d%H%M%S")
LOGFILE="metrics_agg_$NOW.log";
```
Membuat directory dengan yang respective dari `/home/{user}` dengan syntax `~`, lalu buat directory `log` pada path tersebut.
Format waktu yang diiingin didapatkan dengan  perintah `NOW=$(date +"%Y%m%d%H%M%S")` lalu `LOGFILE` akan menyimpan format penamaan file sesuai permintaan soal.

### 3.B
Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.

Berikut adalah contoh isi dari file metrics yang dijalankan tiap menit: 
#### Pembahasan
``` bash
(crontab -l; echo  "* * * * * "~/minute_log.sh) | awk '!x[$0]++' | crontab -
```
Perintah diatas adalah perintah untuk menjalan crontab dari sebua file program (.sh)
`echo  "* * * * * "~/minute_log.sh`
digunakan untuk mencetak perintah cronjob yang menjalankan program  `minute_log.sh`  setiap menit,  ke list crontab pada `crontab -l` .
`| awk '!x[$0]++'` dilakukan untuk memastika cronjob tidak duplikat. lalu cronjob dijalankan.

### 3.C
Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log
>Berikut adalah contoh isi dari file metrics yang dijalankan tiap menit: mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size 15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M 

>Berikut adalah contoh isi dari file aggregasi yang dijalankan tiap jam: type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_us ed,swap_free,path,path_size minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,6

#### Pembahasan
- **metrics_log.sh**

`free -m |awk '{out="mem_"$1; for(i=2;i<=NF;i++){out=out",mem_"$i} ; if(NR==1) printf "%s", out}'> $LOGFILE`
Perintah diatas dilakukan untuk mencetak format baris pertama yaitu 
>mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available

caranya membaca baris pada output perintah `free -m` lalu dengan awk mencetak awalan `mem_` lalu argumen pertama pertama hingga terakhir lalu disimpan pada variable `out` namun hanya baris pertama yang dibaca karena hanya baris pertama yang berisi format data tersebut yaitu saat `if(NR==1)` lalu cetak hasilnya dan simpan pada file `$LOGFILE` yang format nama filenya  telah sesuai.
<br>
`free -m |awk '{out="swap_"$1; for(i=2;i<=3;i++){out=out",swap_"$i} if(NR==1) printf "%s", out}' >> $LOGFILE`
Hal yang sama, namun digunakan hanya untuk mencetak 
> swap_total,swap_used,sw ap_free

<br>

`printf  "path,path_size\n">> $LOGFILE`
untuk mencetak 
> path,path_size 

<br>

```bash
free -m | awk '{out=$2; for(i=3;i<=NF;i++){out=out","$i} ;if(NR>1) printf "%s", out}' >> $LOGFILE
du -sh ~|awk '{print ","$2"/,"$1}'>> $LOGFILE
```
Hal yang serupa hanya saja dilakukan untuk baris ke 2 dan ke 3 dari `free -m` dan `du ~sh ~`, `~ = /home/{user}`
Semua dicetak dengan pemisah berupa koma dan dicetak ke $LOGFILE

<br>

`mv $LOGFILE ~/log`
Menyimpan file yang dihasilakan ke directory /home/{user}/log sesuai permintaan soal.

- **aggregate_minutes_to_hourly_log.sh**
`
NOW=$(date +"%Y%m%d%H");
LOGFILE="metrics_agg_$NOW.log";
`
Hal yang sama seperti sebelumnya mehasilkan penamaan sesuai format dan waktu namun hanya sampai ketelitian jam.

**Implementasi**
``` bash
cat ~/log/metrics_2*.log | awk 'FS="," {if(NR%2==0){
if(NR==2){
	for(i=1;i<=NF;i++) {
		min[i]=$i;max[i]=$i;avg[i]=$i
	}
}else{
for(j=1;j<=NF;j++){
if(min[j]>$j){
	min[j]=$j
}

if(max[j]<$j){
	max[j]=$j
}

if(j!=9)

avg[j]=avg[j]+$j

}

}

}};

END{

for(line=1;line<=3;line++){

for(k=1; k<=NF; k++){

if(k!=NF){

if(line==1)

printf "%s,",min[k]

else if(line==2)

printf "%s,",max[k]

else if(line==3 && k!=9)

printf "%.2f;",(avg[k]/(NR/2))

else if(line==3 && k==9)

printf "%s;", avg[k]

}else{

if(line==1)

printf "%s ",min[k]

else if(line==2)

printf "%s ",max[k]

else if(line==3 && k!=9)

printf "%.2f ", (avg[k]/(NR/2))

}

}
}}' |

awk 'END{print "minimum,"$1,"\nmaximum,"$2,"\navarage;"$3}' > temp.txt
```
- note harap baca melalui file .sh untuk formating kode yang lebih baik, terima kasih, mohon maaf atas kekurangannya
Peritnah diatas ditujukan untuk menghitung nilai minimum, maksimum, dan avg dari semua file yang diawali dengan `metrics_2*.log` dengan ekstensi .log pada folder log.
Lalu dibuat array `min[]` yang menyimpan nilai2 yang minimum disemua baris yang ada/dicetak, hanya baris genap yang dibaca karena hanya baris genap yang mengandung data dari setiap file dengan isi setiap file seperti berikut
`mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,sw ap_free,path,path_size 15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M`
Bisa dilihat hanya baris kedua yang berisi data.
Lalu lakukan iterasi untuk semua baris dan bandingkan isinya array min[] hanya menyimpan nilai terkecil dari setiap baris data, aray max[] hanya menyimpan data terbesar dari setiap baris data.
dan avg[] pertama menyimpan semua total baris data kecuali pada argumen ke-9 yang hanya hanya disimpan karena merupak string berupa `/home/{user}`
. Selanjutnya hasil total dari setiap field pada avg akan di operasikan dengan `printf "%.2f;",(avg[k]/(NR/2))`
Dengan NR/2 adalah banyaknya data karena 1 data memiliki 2 baris output.
selanjutnya hasilnya disimpan pada file temp.txt dengan dipisahkan sepasi mengunakan awk.

Khusus untuk avg, file separator yang digunakan adalah `;` karena nilai decima secara default dipisahkan dengan koma bukan titik. Akan diperbaiki pada perintah selanjutnya nanti.
<br>

```bash
touch $LOGFILE
free -m |awk '{out="mem_"$1; for(i=2;i<=NF;i++){out=out",mem_"$i} if(NR==1) printf "type,%s", out}' > $LOGFILE
free -m |awk '{out="swap_"$1; for(i=2;i<=3;i++){out=out",swap_"$i} if(NR==1) printf "%s;",out}'>> $LOGFILE
printf  "path,path_size\n">> $LOGFILE
```
Buat fielnya dengan touch, lalu cetak formatnya dengan cara yang sama pada file pada minut_log.sh
sehingga dihasilkan format 
>type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_us ed,swap_free,path,path_size

```bash
awk '{FS=OFS=";"} {if(NR==3) gsub(/\,/, ".", $0)} 1' temp.txt |
awk '{FS=OFS=";"} {if(NR==3) gsub(/\;/, ",", $0)} 1' >> $LOGFILE
```
Hal ini khusus dilakukan pada baris ketiga (avg) di temp.txt yang telah berisi data sebelumnya yang masuih dipisahkan dengan `;` dan bilangan desimal yang masih dipsahkan koma. Perintah diatas memanfaatkan `gsub()` pada awk untuk mengubah suatu karakter ke karakter lain. Pertama ubah karakter `,` ke `.` agar desimal dipisah kan dengan titik. Selanjutnya ubah `;` ke `,` agar pemisah antar argumennya menjadi koma.
Dengan begitu outpu file sudah sesuai dengan yang diinginkan.
dan masukkan semua output file dari temp.txt ke file$LOGFILE

`rm temp.txt`
lalu hapu file temp.txt yang sudah tidak digunakan tersebut
`mv $LOGFILE ~/log`
pindahkan logfile ke directory LOG

`(crontab -l ; echo  "0 * * * * "~/aggregate_minutes_to_hourly_log.sh) | awk '!x[$0]++' | crontab -`
Terakhir buat crontab yang akan menjalankan file `aggregate_minutes_to_hourly_log.sh` setiap 1 jam.

### 3.D
Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

#### Pembahasan
sebelum memmindahakan $LOGFILE untuk kedua program. Lakukan perintah `chown $USER  $LOGFILE` untuk menandkan kepunyaan file ada pad User saat itu.

### Kendala
- Kesulitan dalam menghitung nilai min, max, dan avg dari semua fila metric tiap menit
- Hasil avg yang bernilai desimal dipisahkan dengan `,` sehingga harus dilakukan operasi tambahan untuk memastikan file memilik output yang sesuai untuk tetapi memilik separtor berupa koma dan desimal dipisahkan dengan titik.


