#!/bin/bash

read -p "Username: " username

egrep "^$username " ./users/user.txt >/dev/null

if [ $? -eq 0 ]; then
        read -s -p 'Password: ' password
        egrep "^$username $password$" ./users/user.txt >/dev/null
        if [ ! $? -eq 0 ]; then
                echo "Incorrect!"
                date '+%m/%d/%y %H:%M:%S' >> log.txt
                echo "LOGIN: ERROR Failed login attempt on user $username" >> log.txt
                exit 1
        fi
else
        echo "Username does not exist!"
        exit 1
fi

date '+%m/%d/%y %H:%M:%S' >> log.txt
echo "LOGIN: INFO User $username logged in" >> log.txt

echo "Ketik 2 command: dl N untuk download dan att untuk menampilkan percobaan login"

read -r command N

if [ $command == "dl" ]; then
        date=$(date +%Y-%m-%d)
        folder=$(echo "${date}_$username")
        if [ ! -f "$folder".zip ]; then
                mkdir -p $folder
                i=1
                while [ $i -le $N ]
                do
                        echo -e "download ke-$i"
                        if [ $i -lt 10 ]; then
                                wget https://loremflickr.com/320/240 -O ./"$folder"/PIC_"0$i"
                        else
                                wget https://loremflickr.com/320/240 -O ./"$folder"/PIC_"$i"
                        fi
                        i=$((i+1))
                done
                zip -P $password -r $folder.zip $folder
                rm -r $folder
        else
                unzip -P $password $folder.zip
                jml_file=$(ls $folder | wc -l)
                j=$(($jml_file + 1))
                N=$(($jml_file + $N))
                while [ $j -le $N ]
                do
                        echo -e "download ke-$j"
                        if [ $j -lt 10 ]; then
                                wget https://loremflickr.com/320/240 -O ./"$folder"/PIC_"0$j"
                        else
                                wget https://loremflickr.com/320/240 -O ./"$folder"/PIC_"$j"
                        fi
                        j=$((j+1))
                done
                zip -P $password -r $folder.zip $folder
                rm -r $folder
        fi
elif [ $command == "att" ]; then
        berhasil=$(egrep "$username" log.txt | egrep "LOGIN: INFO" | wc -l)
        echo "Percobaan login berhasil: $berhasil"
        gagal=$(egrep "$username" log.txt | egrep "LOGIN: ERROR" | wc -l)
        echo "Percobaan login gagal: $gagal"
else
  echo "Tidak ada command yang sesuai!"
fi
