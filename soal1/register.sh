#!/bin/bash

if [ $(id -u) -eq 0 ]; then
	read -p "Enter username : " username
	read -s -p "Enter password : " password
	length=${#password}
	egrep "^$username" /etc/passwd >/dev/null
	if [ $? -eq 0 ]; then
		echo "REGISTER: ERROR User already exists"
		date '+%m/%d/%y %H:%M:%S' >> log.txt
		echo "REGISTER: ERROR User already exists" >> log.txt
		exit 1
	elif [ $password == $username ]; then
		echo "REGISTER: ERROR Password does not meet criteria"
		date '+%m/%d/%y %H:%M:%S' >> log.txt
		echo "REGISTER: ERROR Password does not meet criteria" >> log.txt
		exit 1
	elif [ $length -ge 8 ] && [[ ! $password =~ ^[a-zA-Z0-9]*[a-z][A-Z][a-zA-Z0-9]*$ && ! $password =~ ^[a-zA-Z0-9]*[A-Z][a-z][a-zA-Z0-9]*$ ]]; then
		echo "REGISTER: ERROR Password does not meet criteria"
		date '+%m/%d/%y %H:%M:%S' >> log.txt
		echo "REGISTER: ERROR Password does not meet criteria" >> log.txt
		exit 1
	elif [ $length -lt 8 ]; then
		echo "REGISTER: ERROR Password does not meet criteria"
		date '+%m/%d/%y %H:%M:%S' >> log.txt
		echo "REGISTER: ERROR Password does not meet criteria" >> log.txt
		exit 1
	else
		pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
		useradd -m -p "$pass" "$username"
		if [ $? -eq 0 ]; then
			echo "REGISTER: INFO User $username registered successfully"
			date '+%m/%d/%y %H:%M:%S' >> log.txt
			echo "REGISTER: INFO User $username registered successfully" >> log.txt
		else
			echo "Failed to add a user!"
		fi
	fi
else
	echo "Only root may add a user to the system."
	exit 2
fi

if [ ! -d "users/" ]
then
	mkdir users
	cd users/
	touch user.txt
	echo "$username $password" >> user.txt
else
	cd users/
	echo "$username $password" >> user.txt
fi
