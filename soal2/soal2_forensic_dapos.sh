#!/bin/bash
#mkdir forensic_log_website_daffainfo_log
#touch ratarata.txt
#touch result.txt

(awk '{system("mkdir -p forensic_log_website_daffainfo_log")} END{ cnt = (NR-1)/12;
print "Rata-rata serangan adalah sebanyak", 
cnt, "request per jam "}' log_website_daffainfo.log) > ratarata.txt

(awk -F\" '{print $2}' log_website_daffainfo.log |
uniq -c | sort -n | sort -rn | head -n 1 |
awk '{print "IP yang paling banyak mengakses server adalah:",$2, 
"sebanyak",$1, "requests\n"}') > result.txt

(awk -F\" '/curl/ {print $8} ' log_website_daffainfo.log | uniq  -c |
awk '{print"ada",
$1,"request yang mengunakan curl sebagai user agent \n"}')>> result.txt

awk -F\" '/\:02\:/ {print $2}' log_website_daffainfo.log |uniq  >> result.txt

mv ratarata.txt forensic_log_website_daffainfo_log/
mv result.txt forensic_log_website_daffainfo_log/
